    import java.util.Random;
    import java.util.Scanner;

    public class GameLogic {

        public static void startTheGame(Scanner scanner, Board gameBoard, String[][] board, Random random, String pieceX, String piece0) {
            while (true) {
                System.out.println("Choose game mode:");
                System.out.println("1. Player vs Player");
                System.out.println("2. Player vs Computer");
                System.out.println("3. Exit game.");
                int gameModeChoose = scanner.nextInt();
                GameLogic.gameMode1v1(gameModeChoose, scanner, gameBoard, board, random, pieceX, piece0);
                GameLogic.gameModePlayerVsComputer(gameModeChoose, scanner, board, random, pieceX, piece0, gameBoard);

                if (gameModeChoose == 3) {
                    break;
                }
            }
        }

        private static void gameModePlayerVsComputer(int gameModeChoose, Scanner scanner, String[][] board, Random random, String pieceX, String piece0, Board gameBoard) {
            if (gameModeChoose == 2) {
                System.out.println("Wprowadź imię gracza");
                String player1Name = scanner.next();
                Player player1 = new Player(player1Name, false, 12);
                Player player2 = new Player("Computer", true, 12);
                Board.boardDisplay(board);
                GameLogic.random1v1GameStart(random, player1, pieceX, player2, piece0);
                boolean gameEnd = false;

                while (true) {
                    if (GameLogic.isGameFinished(player1, player2))
                        break;
                    if (player1.isPlayerTurn() && player1.getPieces().equals(pieceX)) {
                        GameLogic.makeMove(scanner, board, player1, gameBoard, player2, gameEnd);
                    }
                    else if (player1.isPlayerTurn() && player1.getPieces().equals(piece0) ) {
                        GameLogic.makeMove(scanner, board, player1, gameBoard, player2, gameEnd);
                    }
                    else if (player2.isPlayerTurn() && player2.getPieces().equals(pieceX) && player2.getIsPlayerComputer()) {
                        ComputerMovesLogic.makeComputerMove(board, player2, player1);
                    }
                    else if (player2.isPlayerTurn() && player2.getPieces().equals(piece0) && player2.getIsPlayerComputer()) {
                        ComputerMovesLogic.makeComputerMove(board, player2, player1);
                    }
                }

            }
        }

        private static void gameMode1v1(int gameModeChoose, Scanner scanner, Board gameBoard, String[][] board, Random random, String pieceX, String piece0) {
            if (gameModeChoose == 1) {
                System.out.println("Wprowadź imię pierwszego gracza");
                String player1Name = scanner.next();
                Player player1 = new Player(player1Name);
                System.out.println("Wprowadź imię drugiego gracza");
                String player2Name = scanner.next();
                Player player2 = new Player(player2Name);

                Board.boardDisplay(board);
                GameLogic.random1v1GameStart(random, player1, pieceX, player2, piece0);
                boolean gameEnd = false;
                player1.setPlayerPiecesNumber(12);
                player2.setPlayerPiecesNumber(12);

                while (true) {
                    if (GameLogic.isGameFinished(player1, player2))
                        break;
                    if (player1.isPlayerTurn() && player1.getPieces().equals(pieceX)) {
                        GameLogic.makeMove(scanner, board, player1, gameBoard, player2, gameEnd);
                    }
                    else if (player1.isPlayerTurn() && player1.getPieces().equals(piece0) ) {
                        GameLogic.makeMove(scanner, board, player1, gameBoard, player2, gameEnd);
                    }
                    else if (player2.isPlayerTurn() && player2.getPieces().equals(pieceX)){
                        GameLogic.makeMove(scanner, board, player2, gameBoard, player1, gameEnd);
                    }
                    else if (player2.isPlayerTurn() && player2.getPieces().equals(piece0)) {
                        GameLogic.makeMove(scanner, board, player2, gameBoard, player1, gameEnd);
                    }

                }
            }
        }
        public static void makeMove(Scanner scanner, String[][] board, Player player1, Board gameBoard, Player player2, boolean gameEnd) {
                System.out.println("First you have to write from where you want to move, then you have to write where to you want to move. For example:");
                System.out.println("1,2");
                System.out.println("2,3");
                System.out.println("Type which pawn you want to move");
                scanner.nextLine();
                String moveFrom = scanner.next();
                String[] moveFromSplit = moveFrom.split(",");
                if (moveFromSplit.length != 2) {
                    System.out.println("Invalid input. Please enter coordinates in the format 'x,y'");
                    return;
                }
                int columnFrom = Integer.parseInt(moveFromSplit[0]) - 1;
                int rowFrom = Integer.parseInt(moveFromSplit[1]) - 1;
                if (isMoveWithinBoard(columnFrom, rowFrom))
                    return;
                System.out.println("Type where you want to move");
                String moveTo = scanner.next();
                String[] moveToSplit = moveTo.split(",");
                if (moveToSplit.length != 2) {
                    System.out.println("Invalid input. Please enter coordinates in the format 'x,y'");
                    return;
                }
                int columTo = Integer.parseInt(moveToSplit[0]) - 1;
                int rowTo = Integer.parseInt(moveToSplit[1]) - 1;
                if (isMoveWithinBoard(columTo, rowTo))
                    return;
                int isThereEnemyCol = Math.abs((columnFrom + columTo) / 2);
                int isThereEnemyRow = Math.abs((rowFrom + rowTo) / 2);
                if (isMoveToBeatValid(rowFrom, rowTo, columnFrom, columTo)) {
                    if (board[isThereEnemyRow][isThereEnemyCol].equalsIgnoreCase("[ ]")) {
                        System.out.println("Wrong move, there is no opponent to beat. You can move only by one position!");
                    } else if (board[isThereEnemyRow][isThereEnemyCol].equalsIgnoreCase(player2.getPieces())) {
                        capturingPawn(board, player1, gameBoard, player2, rowFrom, columnFrom, isThereEnemyRow, isThereEnemyCol, rowTo, columTo);
                    }
                } else if (board[rowFrom][columnFrom].equalsIgnoreCase(player1.getPieces())) {
                    movingPawn(board, player1, gameBoard, player2, rowFrom, columnFrom, columTo, rowTo);
                } else {
                    System.out.println("Wrong move! You can only move " + player1.getPieces() + " pawns!");
                }
            }


        private static void movingPawn(String[][] board, Player player1, Board gameBoard, Player player2, int rowFrom, int columFrom, int columnTo, int rowTo) {
            board[rowFrom][columFrom] = "[ ]";
            if (isMoveDiagonally(columFrom, columnTo, rowFrom, rowTo))
                return;
            board[rowTo][columnTo] = player1.getPieces();
            Board.boardDisplay(board);
            playerTurnChange(player1, player2);
            System.out.println("It's player: " + player2.getPlayerName() + " turn");
        }

        private static void capturingPawn(String[][] board, Player player1, Board gameBoard, Player player2, int rowFrom, int columnFrom, int isThereEnemyRow, int isThereEnemyColumn, int rowTo, int columnTo) {
            board[rowFrom][columnFrom] = "[ ]";
            board[isThereEnemyRow][isThereEnemyColumn] = "[ ]";
            board[rowTo][columnTo] = player1.getPieces();
            gameBoard.boardDisplay(board);
            if (player1.isPlayerTurn() && player1.getPieces().equals("[X]")) {
                player2.setPlayerPiecesNumber(player2.getPlayerPiecesNumber() - 1);
            } else if (player1.isPlayerTurn() && player1.getPieces().equals("[0]")) {
                player2.setPlayerPiecesNumber(player2.getPlayerPiecesNumber() - 1);
            } else if (player2.isPlayerTurn() && player2.getPieces().equals("[X]")) {
                player1.setPlayerPiecesNumber(player1.getPlayerPiecesNumber() - 1);
            } else if (player2.isPlayerTurn() && player2.getPieces().equals("[0]")) {
                player1.setPlayerPiecesNumber(player1.getPlayerPiecesNumber() - 1);
            }
            playerTurnChange(player1, player2);
            System.out.println("It's player: " + player2.getPlayerName() + " turn");
        }


        private static boolean isMoveDiagonally(int rowFrom, int rowTo, int columnFrom, int columnTo) {
            int isRowValid = Math.abs(rowFrom - rowTo);
            int isColValid = Math.abs(columnFrom - columnTo);
            if (isRowValid < 1 || isColValid < 1) {
                    System.out.println("Wrong move! You can only move diagonally!");
                    return true;
                }
            return false;
        }

        private static boolean isMoveToBeatValid(int rowFrom, int rowTo, int columnFrom, int columnTo) {
            int isRowToBeatValid = Math.abs(rowFrom - rowTo);
            int isColToBeatValid = Math.abs(columnFrom - columnTo);
            return isRowToBeatValid == 2 || isColToBeatValid == 2;
        }

        private static boolean isMoveWithinBoard(int rowTo, int colTo) {
            if (rowTo > 7 || colTo > 7 || rowTo < 0 || colTo < 0) {
                    System.out.println("You tried to move outside board. Try again!");
                    return true;
            }
            return false;
        }

        public static void playerTurnChange(Player player1, Player player2) {
            if (player1.isPlayerTurn()) {
                player1.setPlayerTurn(false);
                player2.setPlayerTurn(true);
            } else if (player2.isPlayerTurn()) {
                player2.setPlayerTurn(false);
                player1.setPlayerTurn(true);
            }
        }

        public static void random1v1GameStart(Random random, Player player1, String pieceX, Player player2, String piece0) {
            boolean whoStarts = random.nextBoolean();
            if (whoStarts) {
                System.out.println("Player: " + player1.getPlayerName() +" starts with 'X' pawns");
                player1.setPlayerTurn(true);
                player1.setPieces(pieceX);
            } else {
                System.out.println("Player: " + player2.getPlayerName() +" starts with 'X' pawns");
                player2.setPlayerTurn(true);
                player2.setPieces(pieceX);
            }
            if (!player1.isPlayerTurn()) {
                player1.setPieces(piece0);
            } else if (!player2.isPlayerTurn()) {
                player2.setPieces(piece0);
            }
        }

        private static boolean isGameFinished(Player player1, Player player2) {
            if (player1.getPlayerPiecesNumber() == 0) {
                System.out.println("Koniec gry! Wygrał gracz: " + player2.getPlayerName());
                return true;
            } else if (player2.getPlayerPiecesNumber() == 0) {
                System.out.println("Koniec gry! Wygrał gracz: " + player1.getPlayerName());
                return true;
            }
            return false;
        }
    }
