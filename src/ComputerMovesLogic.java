import java.util.ArrayList;
import java.util.List;

public class ComputerMovesLogic {
    public static void makeComputerMove(String[][] board, Player computerPlayer, Player humanPlayer) {
        List<ComputerMove> regularMoves = new ArrayList<>();
        List<ComputerMove> capturingMoves = new ArrayList<>();

        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[row].length; col++) {

                if (board[row][col].equals(computerPlayer.getPieces())) {

                    if (computerPlayer.getPieces().equals("[X]")) {
                        int firstRowToMove = row - 1;
                        int firstColToMove = col + 1;
                        int secondRowToMove = row - 1;
                        int secondColToMove = col - 1;

                        int firstRowToBeat = row - 2;
                        int firstColToBeat = col + 2;
                        int secondRowToBeat = row - 2;
                        int secondColToBeat = col - 2;

                        isThereSpaceToBeat(board, humanPlayer, firstRowToMove, firstColToMove, firstRowToBeat, firstColToBeat, capturingMoves, row, col);
                        isThereSpaceToBeat(board, humanPlayer, secondRowToMove, secondColToMove, secondRowToBeat, secondColToBeat, capturingMoves, row, col);

                        isThereSpaceToMove(board, firstRowToMove, firstColToMove, regularMoves, row, col);
                        isThereSpaceToMove(board, secondRowToMove, secondColToMove, regularMoves, row, col);
                    }
                    if (computerPlayer.getPieces().equals("[0]")) {
                        int firstRowToMove = row + 1;
                        int firstColToMove = col + 1;
                        int secondRowToMove = row + 1;
                        int secondColToMove = col - 1;
                        System.out.println("Considering moves for [0] at rowTo1: " + firstRowToMove + ", firstColToMove: " + firstColToMove);
                        System.out.println("Considering moves for [0] at rowTo2: " + secondRowToMove + ", colTo2: " + secondColToMove);


                        int firstRowToBeat = row + 2;
                        int firstColToBeat = col + 2;
                        int secondRowToBeat = row + 2;
                        int secondColToBeat = col - 2;


                        isThereSpaceToBeat(board, humanPlayer, firstRowToMove, firstColToMove, firstRowToBeat, firstColToBeat, capturingMoves, row, col);
                        isThereSpaceToBeat(board, humanPlayer, secondRowToMove, secondColToMove, secondRowToBeat, secondColToBeat, capturingMoves, row, col);

                        isThereSpaceToMove(board, firstRowToMove, firstColToMove, regularMoves, row, col);
                        isThereSpaceToMove(board, secondRowToMove, secondColToMove, regularMoves, row, col);
                    }
                }
            }
        }

        if (!capturingMoves.isEmpty()) {
            int randomMove = (int) (Math.random() * capturingMoves.size());
            System.out.println("Liczba dostępnych ruchow do bicia:" + capturingMoves.size());
            ComputerMove computerMove = capturingMoves.get(randomMove);
            int capturedRow = (computerMove.getStartRow() + computerMove.getEndRow()) / 2;
            int capturedCol = (computerMove.getStartCol() + computerMove.getEndCol()) / 2;
            board[computerMove.getStartRow()][computerMove.getStartCol()] = "[ ]";
            board[capturedRow][capturedCol] = "[ ]";
            humanPlayer.setPlayerPiecesNumber(humanPlayer.getPlayerPiecesNumber() - 1);
            board[computerMove.getEndRow()][computerMove.getEndCol()] = computerPlayer.getPieces();
            GameLogic.playerTurnChange(computerPlayer, humanPlayer);
            Board.boardDisplay(board);

        } else {
            int randomMove = (int) (Math.random() * regularMoves.size());
            System.out.println("Liczba dostępnych regularnych ruchów:" + regularMoves.size());
            ComputerMove computerMove = regularMoves.get(randomMove);
            board[computerMove.getStartRow()][computerMove.getStartCol()] = "[ ]";
            board[computerMove.getEndRow()][computerMove.getEndCol()] = computerPlayer.getPieces();
            System.out.println("Computer moved from row " + computerMove.getStartRow() + " and col " + computerMove.getStartCol() + " to row " + computerMove.getEndRow() + " and col " + computerMove.getEndCol());
            GameLogic.playerTurnChange(computerPlayer, humanPlayer);
            Board.boardDisplay(board);
        }

    }

    private static void isThereSpaceToMove(String[][] board, int firstRowToMove, int firstColToMove, List<ComputerMove> regularMoves, int row, int col) {
        if (isMoveWithinBoard(firstRowToMove, firstColToMove) && board[firstRowToMove][firstColToMove].equals("[ ]")) {
            System.out.println("Adding a valid move for [X] at firstRowToMove: " + firstRowToMove + ", firstColToMove: " + firstColToMove);
            regularMoves.add(new ComputerMove(row, col, firstRowToMove, firstColToMove));
        }
    }

    private static void isThereSpaceToBeat(String[][] board, Player humanPlayer, int firstRowToMove, int firstColToMove, int firstRowToBeat, int firstColToBeat, List<ComputerMove> capturingMoves, int row, int col) {
        if (isMoveWithinBoard(firstRowToMove, firstColToMove) && board[firstRowToMove][firstColToMove].equals(humanPlayer.getPieces())) {

            if (isMoveWithinBoard(firstRowToBeat, firstColToBeat) && board[firstRowToBeat][firstColToBeat].equals("[ ]")) {
                capturingMoves.add(new ComputerMove(row, col, firstRowToBeat, firstColToBeat));
            }
        }
    }

    private static boolean isMoveWithinBoard(int rowTo, int colTo) {
        return rowTo >= 0 && rowTo <= 7 && colTo >= 0 && colTo <= 7;
    }


}