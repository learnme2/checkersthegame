public class Board {
    public void boardStartingPostions(String[][] board) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                board[i][j] = "[ ]";
            }
        }
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if ((i + j) % 2 == 1) {
                    if (i < 3 ) {
                        board[i][j] = "[0]";
                    } else if (i > 4) {
                        board[i][j] = "[X]";
                    } else {
                        board[i][j] = "[ ]";
                    }
                }
            }
        }
    }

    public static void boardDisplay(String[][] board) {
        System.out.println("   1   2   3   4   5   6   7   8   X");
        for (int i = 0; i < 8; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < 8; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("Y");
    }




}
