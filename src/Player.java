public class Player {
    private String playerName;
    private String pieces;
    private boolean playerTurn;
    private int playerPiecesNumber;
    private boolean isPlayerComputer;

    public boolean getIsPlayerComputer() {
        return isPlayerComputer;
    }

    public void setIsPlayerComputer(boolean isPlayerComputer) {
        this.isPlayerComputer = isPlayerComputer;
    }

    public int getPlayerPiecesNumber() {
        return playerPiecesNumber;
    }

    public void setPlayerPiecesNumber(int playerPiecesNumber) {
        this.playerPiecesNumber = playerPiecesNumber;
    }

    public boolean isPlayerTurn() {
        return playerTurn;
    }

    public void setPlayerTurn(boolean playerTurn) {
        this.playerTurn = playerTurn;
    }

    public String getPieces() {
        return pieces;
    }

    public void setPieces(String pieces) {
        this.pieces = pieces;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Player(String playerName, String pieces, boolean playerTurn, boolean isPlayerComputer) {
        this.playerName = playerName;
        this.pieces = pieces;
        this.playerTurn = playerTurn;
        this.isPlayerComputer = isPlayerComputer;
    }

    public Player(String playerName, boolean isPlayerComputer, int playerPiecesNumber) {
        this.playerName = playerName;
        this.isPlayerComputer = isPlayerComputer;
        this.playerPiecesNumber = playerPiecesNumber;
    }

    public Player(String playerName) {
        this.playerName = playerName;
    }
    public Player() {
    }
}
