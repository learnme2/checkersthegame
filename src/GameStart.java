import javax.swing.*;
import java.util.Random;
import java.util.Scanner;


public class GameStart {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        Board gameBoard = new Board();
        String[][] board = new String[8][8];
        gameBoard.boardStartingPostions(board);
        final String pieceX = "[X]";
        final String piece0 = "[0]";
        GameLogic.startTheGame(scanner, gameBoard, board, random, pieceX, piece0);
    }



}


